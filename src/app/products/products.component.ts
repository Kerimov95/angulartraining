import { TranslationWidth } from '@angular/common';
import { templateJitUrl } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Product } from '../models/product';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  addVisible = false;
  product: Product;

  products: Product[] =
    [
      { id: 1, name: 'Мясо', price: 100, description: 'Свежее мясо говядины от ведущих производителей', image: 'https://www.kuharka.ru/images/users/gallery/2010/09/24/82026_600x0_center.jpg' },
      { id: 2, name: 'Мясо курицы', price: 150, description: 'Свежее мясо говядины от ведущих производителей', image: 'https://www.kuharka.ru/images/users/gallery/2010/09/24/82026_600x0_center.jpg' },
      { id: 3, name: 'Помидоры', price: 200, description: 'Свежее мясо говядины от ведущих производителей', image: 'https://www.kuharka.ru/images/users/gallery/2010/09/24/82026_600x0_center.jpg' },
    ]

  constructor() { }

  ngOnInit(): void {

  }

  add() {
    this.product = new Product();
    this.addVisible = true;
  }

  createProduct(product: Product) {
    this.products.unshift(product);
    this.addVisible = false;
  }

}
