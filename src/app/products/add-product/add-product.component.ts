import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Product } from 'src/app/models/product';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  @Output() createEmit = new EventEmitter<Product>() //3 раздел 23 урок
  @Input() product: Product; //3 раздел 22 урок

  constructor() { }

  ngOnInit(): void {
  }

  create() {
    this.createEmit.emit(this.product);
  }

}
