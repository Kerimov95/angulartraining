import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Hero } from '../models/Hero';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  public newHero = new Hero();
  public heros: Hero[]

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get('http://localhost:5005/People').subscribe((data: Hero[]) => this.heros = data)
  }

  public Add() {
    this.http.post("http://localhost:5005/People", this.newHero).subscribe((newHero: Hero) => this.heros.unshift(newHero));
  }

}
